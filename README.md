# Job Offer 


## Archi Diagram

![Job offer archi](docs/_static/job_hunter_archi.png)

## Commandes Docker

#### Afficher les conteneurs up

docker ps

#### Lancer un docker-compose (une stack : l'application entière)

docker-compose up


## AWS

#### Prerequisite

Exporter nos crédentials pour pouvoir se connecter aux api en locale
```bash
#!/bin/bash

export AWS_ACCESS_KEY_ID=<$YOUR_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<$YOUR_SECRET_KEY_ID>
export AWS_DEFAULT_REGION=eu-west-1
export AWS_DEFAULT_OUTPUT=json
```
#### EC2 - ElasticSearch + Kibana

1. Machine EC2 crée et connection ssh via :
```bash
 ssh -i ~/.ssh/<YOUR_SSH_KEY> ubuntu@<IP_MACHINE> 
```
2. Installation de docker sur la machine (Ubuntu:18.04) [doc docker install](https://docs.docker.com/engine/install/ubuntu/)
3. Installation docker-compose via :
```bash
sudo apt-get install docker-compose
```
4. Copy paste le fichier `docker-compose.yml` :

```yaml
version: '3.3'
services:
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.3.2
    container_name: elasticsearch-jobhunter
    environment:
      - node.name=elasticsearch-jobhunter
      - cluster.name=docker-cluster
      - cluster.initial_master_nodes=elasticsearch-jobhunter
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512M -Xmx512M"
      - transport.host=localhost
      - transport.tcp.port=9300
      - http.port=9200
      - http.cors.enabled=true
      - http.cors.allow-origin=*
      - network.host=0.0.0.0
    ulimits:
      nproc: 65535
      memlock:
        soft: -1
        hard: -1
    cap_add:
      - ALL
    # privileged: true
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      resources:
        limits:
          cpus: '1'
          memory: 512M
        reservations:
          cpus: '1'
          memory: 512M
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 10s
    volumes:
      - type: volume
        source: logs
        target: /var/log
      - type: volume
        source: esdata1
        target: /usr/share/elasticsearch/data
    networks:
      - elastic
    ports:
      - 9200:9200
      - 9300:9300
  kibana:
    image: docker.elastic.co/kibana/kibana:7.3.2
    container_name: kibana
    environment:
      SERVER_NAME: localhost
      ELASTICSEARCH_URL: http://elasticsearch-jobhunter:9200/
    ports:
      - 5601:5601
    volumes:
      - type: volume
        source: logs
        target: /var/log
    ulimits:
      nproc: 65535
      memlock:
        soft: -1
        hard: -1
    cap_add:
      - ALL
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      resources:
        limits:
          cpus: '1'
          memory: 256M
        reservations:
          cpus: '1'
          memory: 256M
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 3
        window: 120s
    networks:
      - elastic

volumes:
  esdata1:
  logs:
networks:
  elastic:
```
5. Run le docker-compose via :

```bash
docker-compose up -d # -d for detach mode
```

#### Déploiement de l'api pyhton

On va utiliser [zappa](https://github.com/Miserlou/Zappa#using-custom-aws-iam-roles-and-policies) pour déployer notre app 
flask dans une lambda aws.

1. Installation via `pip install zappa`
2. Pour déployer la stack veuillez runner :

```bash
zappa deploy
```

:warning: Une lambda peut contenir un artifact.zip  < 50 mo, veuillez retirer les dépendances non nécessaire 
et installer celles dont vous avez besoin;

ex: 
```bash
cd <MON_ENV_VIRTUEL_PYTHON>
pip freeze | xargs pip uninstall -y # Pour tout désinstaller
pip -r requirements.txt # Installer les requirements nécessaire
``` 
3. Pour Updater la stack :


```bash
zappa update
```

4. Pour détruire la stack :

```bash
zappa undeploy
```